#+title: Methodology
#+startup:fold
1. Analyse systemic and scripted ethical games.
2. Design systems-driven cyberethics training gameplay
   a. Gameplay system
   b. Genre
3. Design narrative-game cyberethics gameplay
   A. Simulation-based
   B. Narrative Fiction
      a) Branching narrative (Number of branches)
      b) Length
4. Measure player’s motivation in prioritising moral commitments over others (ludic?) between the two types of gameplay
