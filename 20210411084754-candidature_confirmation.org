#+title: Candidature_Confirmation

* Thesis Title
* Project Summary
* Theoretical Orientation
* Literature Review
* Aims, Significance & Expected Outcomes
* Research Question and sub-questions
* Research Design
** Methodology
** Data Collection/Analysis Strategy
* Ethical Considerations
* Bibliography
* Time Frame (Publishing Schedule)
* Funding / Other needs
